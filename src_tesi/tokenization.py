import nltk
import string
import re

class tokenization(object):

    def __init__(self):
        self.tokens = []

    #analizza il file e fa partire la funzione tokenize che serve per tokenizzare il testo
    def tokenize(self, list, language):
        self.tokens = []
        for element in list:
            aToken = self.tokenizeRow(element, language)
            if len(aToken) != 0:
                self.tokens.append(aToken[0])
        return self.tokens

    def tokenizeRow(self,row, language):
        tokens = self.transformText(row,language)
        return tokens

    def transformText(self,t,language):
        texts = t.lower()

        #rimozione punteggiatura
        tokens = self.removePunctuation(texts)

        #ottimizzazione con rimozione stopwords
        tokens = self.removeStopwords(tokens,language)

        #stemmatizzazione
        tokens = self.stemming(tokens,language)

        return tokens

    def removePunctuation(self,t):
        try:
            texts_without_punctuation = "".join(x.encode('utf-8', errors='ignore')
                                                for x in t if x not in string.punctuation)
        except UnicodeEncodeError:
            texts_without_punctuation = "null"
        except UnicodeDecodeError:
            texts_without_punctuation = "null"

        tokens = texts_without_punctuation.split()
        return tokens

    def removeStopwords(self,t,language):
        stopwords = [x.decode('utf-8',errors='ignore').encode('utf-8',errors='ignore')
                     for x in nltk.corpus.stopwords.words(language)]
        tokens = [x for x in t if not x.lower() in stopwords]
        return tokens

    def stemming(self,t,language):
        stemmer = nltk.stem.SnowballStemmer(language)
        try:
            tokens = [stemmer.stem(x) for x in t]
        except UnicodeDecodeError:
            tokens = ["null"]

        return tokens