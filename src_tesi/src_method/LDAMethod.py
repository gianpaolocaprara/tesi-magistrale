from pymongo import MongoClient, ASCENDING
from gensim import corpora
import CorpusLDA
import os

client = MongoClient('localhost', 27017)


def create_corpora(dataset, dict_to_save, corpus_to_save):
    print "Creazione del corpora LDA."

     # funzioni per la creazione del dizionario utenti (a ogni utente sara associato un ID univoco)
    print "Creazione dizionario LDA."
    create_dictionary(dataset, 'idSource', dict_to_save)
    print "Creazione dizionario completato."

    # importo la class MyCorpus per poter creare il corpus
    i = 0
    corpus = CorpusLDA.CorpusLDA(dataset=dataset)
    for line in corpus:
        print str(i) + " : " + str(line)
        i += 1
    print "Creazione corpora completata."
    print "Salvataggio corpora."
    if not os.path.exists('corpus/'):
        os.makedirs('corpus/')
    corpora.MmCorpus.serialize(corpus_to_save, corpus)
    print "Salvataggio completato."
    print "Creazione collezione posizioni locali in corso."
    create_position_business_in_corpora(dataset)
    print "Creazione collezione completata."
    print "Creazione completata."
    return


def create_dictionary(dataset, coll_id, file_name):
    db = client[dataset]
    new_list = []
    user_coll = db['DB_Content']
    pipeline = [{
        "$project": {"_id": 0, coll_id: 1}
    }]
    rows_user = user_coll.aggregate(pipeline)
    for result in rows_user:
        new_list.append([result.values()[0]])
    rows_user.close()
    dictionary = corpora.Dictionary(new_list)
    if not os.path.exists('dict/'):
        os.makedirs('dict/')
    dictionary.save(file_name)
    return


def load(dict_to_load, corpus_to_load):
    print "Caricamento dizionario."
    dictionary = corpora.Dictionary.load(dict_to_load)
    print "Caricamento corpora."
    training_corpus = corpora.MmCorpus(corpus_to_load)
    print "Caricamento oggetti completato."
    return dictionary, training_corpus


def create_position_business_in_corpora(dataset):
    db = client[dataset]
    position_business_in_corpora_coll = db['positionBusinessInCorpora']
    review_diveded_by_id_business_coll = db['votesDivededByIDRif']
    position_business_in_corpora_coll.remove({})
    pipeline = [
            {"$project": {"_id": 1}},
            {"$sort": {"_id": 1}}
        ]
    cursor_results = review_diveded_by_id_business_coll.aggregate(pipeline)
    # inizializzazione dizionario e contatore posizione
    position_business_dictionary = {}
    position = 0
    for element in cursor_results:
        position_business_dictionary[element.values()[0]] = position
        position += 1
    cursor_results.close()
    for k, v in position_business_dictionary.items():
        json_to_insert = {'business_id': k, 'positionInCorpora': v}
        position_business_in_corpora_coll.insert_one(json_to_insert).inserted_id
    return


def create_inverted_components_distribution(dataset, topic_components):
    db = client[dataset]
    components_topic_distribution_LDA_coll = db['components_topic_distribution_LDA']
    components_topic_distribution_LDA_coll.remove({})
    print "Creazione collezione distribuzione inversa delle componenti in corso."
    inverted_component_distribution = {}
    for element in topic_components:
        for item in element[1]:
            try:
                inverted_component_distribution[item[0]].append({'topic': element[0], 'distribution': item[1]})
            except KeyError:
                inverted_component_distribution[item[0]] = [{'topic': element[0], 'distribution': item[1]}]
    for k, v in inverted_component_distribution.items():
        json_to_insert = {'component': k, 'topics_distribution': v}
        components_topic_distribution_LDA_coll.insert_one(json_to_insert).inserted_id
    print "Creazione collezione completata."
    print "Creazione indici."
    components_topic_distribution_LDA_coll.create_index([("component", ASCENDING)])
    components_topic_distribution_LDA_coll.create_index([("topics_distribution", ASCENDING)])
    components_topic_distribution_LDA_coll.create_index([("topics_distribution.distribution", ASCENDING)])
    return


def create_topic_objects_distribution(dataset, corpus):
    db = client[dataset]
    topics_objects_distribution_coll = db['objects_topic_distribution_LDA']
    topics_objects_distribution_coll.remove({})
    print "Creazione collezione distribuzione inversa degli oggetti in corso."
    topic_objects_distribution = {}
    i = 0
    for element in corpus:
        print "Collezione distributioni oggetti, divisi per topic; Iteration: " + str(i)
        # recupero l'ID ristorante associato all'indice
        business_id = get_id_business(dataset, i)
        # incremento il contatore per il prossimo ID
        i += 1
        # scorro la lista delle distribuzioni dei topic del business e aggiungo ogni topic al dizionario
        for tuples in element:
            try:
                topic_objects_distribution[business_id].append({'topic': tuples[0], 'distribution': tuples[1]})
            except KeyError:
                topic_objects_distribution[business_id] = [{'topic': tuples[0], 'distribution': tuples[1]}]
    for k, v in topic_objects_distribution.items():
        json_to_insert = {'idRif': k, 'topics_distribution': v}
        topics_objects_distribution_coll.insert_one(json_to_insert).inserted_id
    print "Creazione collezione completata."
    print "Creazione indici."
    topics_objects_distribution_coll.create_index([("business_distribution.topic", ASCENDING)])
    print "Creazione indici completata."
    return


# ottiene ID del Business data la posizione
def get_id_business(dataset, position):
    db = client[dataset]
    position_business_in_corpora_coll = db['positionBusinessInCorpora']
    business_id = ""
    cursor = position_business_in_corpora_coll.find({"positionInCorpora": position})
    for elem in cursor:
        business_id = elem['business_id']
    return business_id
