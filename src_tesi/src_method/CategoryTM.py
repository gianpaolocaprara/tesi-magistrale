from __future__ import division
from scipy.special import psi
from pymongo import MongoClient

import numpy as n
import Utility
import os

meanchangethresh = 0.001


def dirichlet_expectation(alpha):
    """
    For a vector theta ~ Dir(alpha), computes E[log(theta)] given alpha.
    """
    if len(alpha.shape) == 1:
        return psi(alpha) - psi(n.sum(alpha))
    return psi(alpha) - psi(n.sum(alpha, 1))[:, n.newaxis]


class CategoryTM(object):
    def __init__(self, corpora_dict=None, num_topics=100, cnt_dict=None, iterations=100, dat_to_save='Tesi',
                 topics_to_consider=5, chunklength= 3000, weight_correct_cat=10, weight_incorrect_cat=0.1):

        # settaggio variabili
        self.corpora_dict = corpora_dict
        self.num_topics = int(num_topics)
        self.cnt_dict = cnt_dict
        self.dataset_name = dat_to_save
        self.n_topics_output = topics_to_consider
        self.chunk = chunklength

        self.weight_cor_cat = weight_correct_cat
        self.weight_incor_cat = weight_incorrect_cat

        # dizionari prodotti/documenti e utenti/parole
        self.dictionary_objects, self.dictionary_components, self.cat_dict = Utility.load()

        # lunghezza vocabolario
        self.len_doc = len(self.dictionary_objects)

        self.iterations = iterations

        # iperparametri
        self.alpha = 0.1
        self.beta = 0.01
        self.kappa = 0.75
        self.updatect = 0
        self.tau0 = 1

        # Per salvataggio sul DB #
        self.client = MongoClient('localhost', 27017)
        self.db = self.client[dat_to_save]

        # controllo variabili
        if self.corpora_dict is None and self.cnt_dict is None:
            raise ValueError('Impossibile stabilire la dimensione dello spazio di input. Inserire il corpus dei product'
                             'e il dizionario degli utenti')

        if self.cnt_dict is None:
            raise ValueError('Impossibile continuare. Necessario definire i dizionari')

        if self.num_topics == 0:
            raise ValueError('Impossibile continuare. Definire un numero di topic > 0')

        if self.weight_incor_cat <= 0 and self.weight_incor_cat > 1:
            raise ValueError('Il peso del termine in caso di categoria differente deve essere > 0 e <= 1')

        print "Creazione lambda e gamma"
        # inizializzo i lambda delle categorie
        self.lambda_dict = self.obtain_lambda_distributions()
        print "Creazione lambda completata"
        self.gamma = dict()
        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)
        for element in categories:
            gamma = 1*n.random.gamma(100., 1./100., (self.len_doc, self.num_topics))
            self.gamma[str(element['_id'])] = {'gamma': gamma}
        categories.close()

        # creo la matrice
        print "Creazione matrice."
        self.users_matrix = self.create_matrix()
        print "Matrice completa."

        gamma_chunk = dict()
        print "Inizio il metodo."
        for i in range(int(self.len_doc/self.chunk)+1):
            # prendo il chunk per poter aggiornare successivamente i lambda
            print "Analizzo il chunk da posizione " + str(i*self.chunk)\
                  + " a posizione " + str(((i+1)*self.chunk)-1)
            wordids, wordcts = self.parse_content_objects(i*self.chunk,
                                                          ((i+1)*self.chunk)-1)
            gamma_chunk = self.update_lambda(wordids, wordcts, i*self.chunk)

        # salvataggio risultati
        if not os.path.exists('results/'):
            os.makedirs('results/')
        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)
        # Save lambda, the parameters to the variational distributions over topics
        print "Salvataggio su file"
        for element in categories:
            category = str(element['_id'])
            n.savetxt('results/lambda_%s-%d.dat' % (category, self.iterations),
                      self.lambda_dict[category]['lambda'])
            n.savetxt('results/gamma_%s-%d.dat' % (category, self.iterations),
                      self.gamma[category]['gamma'])

        categories.close()
        print "Salvataggio completato."

        # salva i risultati sul DB
        self.save_topics()

    def update_lambda(self, wordids, wordcts, valA):
        # effettua uno step sugli oggetti del chunk considerato e successivamente
        # utilizza il risultato per aggiornare i parametri lambda delle k distribuzioni

        # rhot will be between 0 and 1, and says how much to weight
        # the information we got from this batch.
        rhot = pow(self.tau0 + self.updatect, -self.kappa)
        self._rhot = rhot

        # Do an E step to update gamma, phi | lambda
        # This also returns the information about phi that
        # we need to update lambda.
        (gamma_dict, sstats_dict) = self.do_e_step(wordids, wordcts, valA)

        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)

        for element in categories:
            category = str(element['_id'])
            # Aggiorna il lambda della categoria presa in considerazione
            self.lambda_dict[category]['lambda'] = \
                self.lambda_dict[category]['lambda'] * (1-rhot) + \
                rhot * (self.beta + self.len_doc *
                        sstats_dict[category]['sstats'] / len(wordids))
            self.lambda_dict[category]['Elogbeta'] = \
                dirichlet_expectation(self.lambda_dict[category]['lambda'])
            self.lambda_dict[category]['expElogbeta'] = \
                n.exp(self.lambda_dict[category]['Elogbeta'])

        categories.close()
        self.updatect += 1

        return gamma_dict

    def do_e_step(self, wordsids, wordscts, valA):
        # Ottiene il set di oggetti del chunk e stima i parametri gammas
        # inerenti alle k distribuzioni, per ogni oggetto presente nel set.
        # Ritorna un dizionario contenente i valori stimati dei k gamma,
        # sufficienti per poter fare statistiche per aggiornare i valori dei k lambda
        batch = len(wordsids)

        gamma_dict, sstats_dict = self.obtain_gamma_and_sttats_distributions(batch)

        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)

        # Per ogni documento p aggiorna gamma e phi, per ogni categoria
        for p in range(0, batch):
            ids = wordsids[p]
            for element in categories:
                category = str(element['_id'])
                cts = wordscts[category][p]
                gammad = gamma_dict[category]['gamma'][p, :]
                Elogthetad = gamma_dict[category]['Elogtheta'][p, :]
                expElogthetad = gamma_dict[category]['expElogtheta'][p, :]
                expElogbetad = self.lambda_dict[category]['expElogbeta'][:, ids]
                sstats = sstats_dict[category]['sstats']
                # The optimal phi_{dwk} is proportional to
                # expElogthetad_k * expElogbetad_w. phinorm is the normalizer.
                phinorm = n.dot(expElogthetad, expElogbetad) + 1e-100

                # Iterate between gamma and phi until convergence
                for _ in xrange(self.iterations):
                    lastgamma = gammad
                    # We represent phi implicitly to save memory and time.
                    # Substituting the value of the optimal phi back into
                    # the update for gamma gives this update. Cf. Lee&Seung 2001.
                    gammad = self.alpha + expElogthetad * n.dot(cts / phinorm, expElogbetad.T)
                    Elogthetad = dirichlet_expectation(gammad)
                    expElogthetad = n.exp(Elogthetad)
                    phinorm = n.dot(expElogthetad, expElogbetad) + 1e-100

                    # If gamma hasn't changed much, we're done.
                    meanchange = n.mean(abs(gammad - lastgamma))
                    if meanchange < meanchangethresh:
                        break

                gamma_dict[category]['gamma'][p, :] = gammad
                self.gamma[category]['gamma'][(valA+p), :] = gammad
                # Contribution of document d to the expected sufficient
                # statistics for the M step.
                sstats[:, ids] += n.outer(expElogthetad.T, cts/phinorm)
                sstats_dict[category]['sstats'] = sstats

        categories.close()
        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)
        for element in categories:
            category = str(element['_id'])
            # This step finishes computing the sufficient statistics for the
            # M step, so that
            # sstats[k, w] = \sum_d n_{dw} * phi_{dwk}
            # = \sum_d n_{dw} * exp{Elogtheta_{dk} + Elogbeta_{kw}} / phinorm_{dw}.
            sstats_dict[category]['sstats'] = sstats_dict[category]['sstats'] \
                                              * self.lambda_dict[category]['expElogbeta']

        categories.close()
        return gamma_dict, sstats_dict

    # ottiene i counter di ogni singolo utente/documento, per ogni locale/documento
    def obtain_source_with_counts(self, rif, sources):
        sources_cnts = []
        for source in sources:
            source_cnts = self.cnt_dict.get(source['component'])
            for cnt in source_cnts:
                if cnt.keys() == [rif]:
                    sources_cnts.append((source['component'], cnt[rif]))
        return sources_cnts

    # funzione che parsa gli elementi del dataset per estrarre le informazioni base
    # Restituisce : wordsids = lista di liste; ogni lista e' un oggetto complesso
    # wordscts = dizionario in cui le chiavi sono le categorie che si vuole analizzare e il valore e' una lista
    # di liste
    def parse_content_objects(self, begin, end):
        db_categories = self.db['DB_Categories']

        wordsids = list()
        wordscts = {}

        # ottengo la lista delle source e la lista delle cnt, per ogni rif presente nel batch
        for object, components in self.corpora_dict.items()[begin:end]:
            # ottiene i conteggi degli elementi all'interno del rif
            sources_counts = self.obtain_source_with_counts(object, components)
            # estraggo gli id
            sources_id_rif = zip(*sources_counts)[0]
            # estraggo i cnt
            sources_cnts_rif = zip(*sources_counts)[1]
            # appendo la lista degli id appartenenti a quel rif all'interno di wordsids
            wordsids.append(sources_id_rif)

            # estraggo l'id del rif
            id_rif = self.dictionary_objects[object]
            # recupero all'interno del DB la categoria del rif
            tuple = db_categories.find_one({'idRif': id_rif})
            # tuple = db_categories.find_one({'_id': id_rif})
            category = tuple['category']

            # lista che conterra', per ogni rif, i valori dei rif da passare alla distribuzione
            final_list_category_correct = []
            # lista che conterra', per ogni rif, i valori dei rif da passare alla distribuzione
            final_list_category_incorrect = []

            # per ogni cnt...
            for i in range(0, len(sources_cnts_rif)):
                # estraggo id source e cnt
                source_id = sources_id_rif[i]
                source_value = sources_cnts_rif[i]

                # recupero l'ID della categoria all'interno del dizionario
                id_cat = self.cat_dict.doc2bow([category])[0][0]
                # considero il valore all'interno della matrice
                value_matrix = self.users_matrix.item(int(source_id), id_cat)

                # calcolo il valore da passare alla distribuzione
                # specificita
                spec = self.specificity(source_value, value_matrix)

                final_value = spec*self.weight_cor_cat
                final_value_2 = self.weight_incor_cat

                # appendo il risultato ottenuto
                final_list_category_correct.append(final_value)
                final_list_category_incorrect.append(final_value_2)

            # inserisco i risultati nel wordscts della categoria considerata e successivamente nelle altre categorie
            try:
                wordscts[str(category)].append(final_list_category_correct)
            except KeyError:
                wordscts[str(category)] = [final_list_category_correct]
            # recupero tutte le categorie
            for k, v in self.cat_dict.items():
                if str(v) != str(category):
                    try:
                        wordscts[str(v)].append(final_list_category_incorrect)
                    except KeyError:
                        wordscts[str(v)] = [final_list_category_incorrect]

        return wordsids, wordscts

    def save_topics(self):
        vocab_components = self.dictionary_components
        vocab_objects = self.dictionary_objects

        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)

        for element in categories:
            category = str(element['_id'])
            testlambda = n.loadtxt('results/lambda_%s-%d.dat' % (category, self.iterations))

            print 'Salvataggio sul DB in corso...'
            components_topic_db = self.db["components_topic_distribution_" + str(element['_id'])]
            # rimuovo i risultati precedenti, se ci sono
            components_topic_db.remove()

            components_distributions = {}

            print 'Salvataggio per ' + str(category)
            for k in range(0, len(testlambda)):
                lambdak = list(testlambda[k, :])
                lambdak = lambdak / sum(lambdak)
                temp = zip(lambdak, range(0, len(lambdak)))
                temp = sorted(temp, key=lambda x: x[0], reverse=True)
                for i in range(0, len(vocab_components)):
                    try:
                        try:
                            components_distributions[vocab_components[temp[i][1]]]\
                                .append({'topic': k, 'distribution': temp[i][0]})
                        except KeyError:
                            components_distributions[vocab_components[temp[i][1]]]\
                                = [{'topic': k, 'distribution': temp[i][0]}]
                    except IndexError:
                        break

            for k, v in components_distributions.items():
                json_to_insert = {'_id': k, 'component_distributions': v}
                components_topic_db.insert_one(json_to_insert)

        categories.close()
        print "Salvataggio componenti completato."
        print "Salvataggio oggetti in corso."

        objects_distribution = dict()
        for i in range(0, self.len_doc):
            print "DOCUMENT " + str(i)
            wordids, wordcts = self.parse_content_objects(i, i+1)
            (gamma_p, _) = self.do_e_step(wordids, wordcts, i)
            categories = db_categories.aggregate(pipeline)
            for element in categories:
                category = str(element['_id'])
                topic_dist = gamma_p[category]['gamma'][0] \
                             / sum(gamma_p[category]['gamma'][0])
                tuple_doc = []
                for topicid, topicvalue in enumerate(topic_dist):
                    tuple_doc.append({'topic': topicid, 'distribution': topicvalue})

                try:
                    objects_distribution[category].append({'id': vocab_objects[i],
                                                           'distribution': tuple_doc})

                except KeyError:
                    objects_distribution[category] = [{'id': vocab_objects[i],
                                                       'distribution': tuple_doc}]
            categories.close()

        categories = db_categories.aggregate(pipeline)
        for element in categories:
            category = str(element['_id'])
            objects_topic_db = self.db["objects_topic_distribution_" + str(element['_id'])]
            objects_topic_db.remove()
            elements_prod = objects_distribution[category]
            for element_prod in elements_prod:
                json_to_insert = {'_id': element_prod['id'],
                                  'object_distributions': element_prod['distribution']}
                objects_topic_db.insert_one(json_to_insert)

        print "Salvataggio completato"

        return

    def create_matrix(self):
        db_categories = self.db['DB_Categories']

        # lista di liste
        matrix_support = []

        # recupero le informazioni dal DB
        pipeline = [
            {"$group": {"_id": "$idSource", "cnts": {"$push": {"idRif": "$idRif", "count": "$cnt"}}}},
            {"$sort": {"_id": 1}},
            {"$out": "collection_for_matrix"},
        ]
        self.db.command(
            {"aggregate":
                 "DB_Content",
             "pipeline":
                 pipeline, "allowDiskUse": True})

        db_4matrix = self.db['collection_for_matrix']
        results = db_4matrix.find()

        support_list = []
        for result in results:
            source_id = result['_id']
            # recupero l'ID della componente all'interno del dizionario
            id_source_rif = self.dictionary_components.doc2bow([source_id])[0][0]
            counts = result['cnts']

            tuple_component = n.zeros((len(self.cat_dict),), dtype=n.int)
            for count in counts:
                id_rif = count['idRif']

                cnt = count['count']
                # recupero all'interno del DB la categoria dell'oggetto
                tuple = db_categories.find_one({'idRif': id_rif})
                # tuple = db_categories.find_one({'_id': id_rif})
                category = tuple['category']
                # recupero l'ID della categoria all'interno del dizionario
                id_cat = self.cat_dict.doc2bow([category])[0][0]
                tuple_component[id_cat] += cnt
            support_list.append([id_source_rif, tuple_component])
        # ordino la lista in maniera tale da poter avere una matrice ordinata
        support_list.sort()
        for element in support_list:
            matrix_support.append(element[1])

        final_matrix = n.matrix(matrix_support)
        return final_matrix

    # restituisce un dizionario con i valori relativi alle distribuzioni, una per ogni categoria
    def obtain_lambda_distributions(self):
        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)
        lambda_distibutions = {}

        # tante distribuzioni quante sono le categorie
        for element in categories:
            # Initialize the variational distribution q(beta|lambda) for category
            _lambda = 1*n.random.gamma(100., 1./100., (self.num_topics,
                                                       len(self.dictionary_components)))
            _Elogbeta = dirichlet_expectation(_lambda)
            _expElogbeta = n.exp(_Elogbeta)
            lambda_distibutions[str(element['_id'])] = \
                {'lambda': _lambda, 'Elogbeta': _Elogbeta, 'expElogbeta': _expElogbeta}
        categories.close()
        return lambda_distibutions

    # restituisce due con i valori relativi a gamma, una per ogni categoria e al vettore sstats, una per ogni
    # categoria
    def obtain_gamma_and_sttats_distributions(self, len_batch):
        db_categories = self.db['DB_Categories']
        pipeline = [{"$group": {"_id": "$category"}}]
        categories = db_categories.aggregate(pipeline)

        gamma_distributions = {}
        sstats_distributions = {}

        # creo tante distribuzioni quante sono le categorie e le inserisco all'interno di un dizionario
        for element in categories:
            # Initialize the variational distribution q(theta|gamma)
            gamma = 1*n.random.gamma(100., 1./100.,
                                     (len_batch, self.num_topics))
            Elogtheta = dirichlet_expectation(gamma)
            expElogtheta = n.exp(Elogtheta)
            gamma_distributions[str(element['_id'])] = \
                {'gamma': gamma, 'Elogtheta': Elogtheta,
                 'expElogtheta': expElogtheta}

            # recupero il lambda della categoria associata
            sstats = n.zeros_like(self.lambda_dict[str(element['_id'])]
                                  ['expElogbeta'])
            sstats_distributions[str(element['_id'])] = {'sstats': sstats}

        categories.close()

        return gamma_distributions, sstats_distributions

    def specificity(self, value_cnt, value_matrix):
        # calcolo specificita
        try:
            spec = (value_cnt - value_matrix)/(n.sqrt(value_matrix))
        except ZeroDivisionError:
            spec = 0
        except TypeError:
            spec = 0

        final_value = 0

        if spec > 0:
            final_value = spec + value_cnt
        elif spec < 0:
            final_value = value_cnt - spec
        elif spec == 0:
            final_value = value_cnt

        return float("{0:.3f}".format(final_value))
