from pymongo import MongoClient
from gensim import corpora

import os


def create_dictionary(dataset):
    # connessione al dataset : il dataset deve contenere le due collezioni "DB_Categories" e "DB_Content"
    client = MongoClient('localhost', 27017)
    db = client[dataset]
    db_categories = db['DB_Categories']
    db_content = db['DB_Content']
    # recupero le informazioni relative ai locali/documento e agli utenti/parole
    rows_source_db = []
    rows_content_db = []
    rows_categories_db = []
    # pipeline = [{"$group": {"_id": "$_id"}}]

    pipeline = [{"$group": {"_id": "$idRif"}}]
    source_aggregation = db_categories.aggregate(pipeline)
    for element in source_aggregation:
        rows_source_db.append([element['_id']])
    source_aggregation.close()

    pipeline = [{"$group": {"_id": "$idSource"}}]
    content_aggregation = db_content.aggregate(pipeline)
    for element in content_aggregation:
        rows_content_db.append([element['_id']])
    content_aggregation.close()

    pipeline = [{"$group": {"_id": "$category"}}]
    content_aggregation = db_categories.aggregate(pipeline)
    for element in content_aggregation:
        rows_categories_db.append([element['_id']])
    content_aggregation.close()
    # creo i dizionari e li salvo
    dictionary_objects = corpora.Dictionary(rows_source_db)
    dictionary_components = corpora.Dictionary(rows_content_db)
    dictionary_categories = corpora.Dictionary(rows_categories_db)
    if not os.path.exists('dict/'):
        os.makedirs('dict/')
    dictionary_objects.save('dict/objects_dict_method.dict')
    dictionary_components.save('dict/components_dict_method.dict')
    dictionary_categories.save('dict/categories_dict_method.dict')
    return


def load():
    print "Caricamento dizionari."
    dictionary_p = corpora.Dictionary.load('dict/objects_dict_method.dict')
    dictionary_u = corpora.Dictionary.load('dict/components_dict_method.dict')
    dictionary_c = corpora.Dictionary.load('dict/categories_dict_method.dict')
    print "Caricamento completato."
    return dictionary_p, dictionary_u, dictionary_c


def content_rows(dataset):
    client = MongoClient('localhost', 27017)
    db = client[dataset]
    db_content = db['DB_Content']
    print "Recupero informazioni dal dataset DB_Content."
    results = []
    # recupero le tuple da DB_Content
    cursor = db_content.find()
    for element in cursor:
        results.append(element)
    return results
