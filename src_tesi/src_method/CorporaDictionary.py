class CorporaDictionary(object):
    def __init__(self, dataset, dict_objects, dict_components):
        self.dataset = dataset
        self.corporaDict = {}
        self.dict_objects = dict_objects
        self.dict_components = dict_components

        if dataset is None:
            raise ValueError("Dataset cannot be empty.")

        # crea il dizionario corpora
        self.create_dictionary()

    def create_dictionary(self):
        for elem in self.dataset:
            id_source = self.dict_components.doc2bow([elem['idSource']])[0][0]
            id_rif = self.dict_objects.doc2bow([elem['idRif']])[0][0]
            try:
                self.corporaDict[id_rif].append({'component': id_source})
            except KeyError:
                self.corporaDict[id_rif] = [{'component': id_source}]
        return
