import evalMethod
import sys
'''
evaluation.py : script che permette di valutare, dato un dataset sulla quale e' stato eseguito lo script compute.py,
    la bonta' del metodo applicato e del modello LDA. Vengono passati come parametri:
    1) Dataset da considerare per la valutazione
    2) n_topic_created: # di topic generati nel precedente metodo compute.py
    3) method: definisce il metodo utilizzato per la valutazione; puo' essere di due tipi:
        - "soglia": utilizzato se si vuole effettuare la valutazione utilizzando una soglia sulla distribuzione;
        - "topic: utilizzato se si vuole effettuare la valutazione utilizzando un # di topic per ogni documento
    4) value: definisce il valore associato al method utilizzato
Usage: python evaluation.py dataset n_topic_created method value
'''
if len(sys.argv) < 3:
    print "Bad command. Usage:"
    print "python evaluation.py dataset n_topic_created method value"
    sys.exit(1)

arguments = sys.argv
dataset = arguments[1]
n_topic_created = int(arguments[2])
method = str(arguments[3])
value_method = arguments[4]

print "*****EVALUATION******"
print "DATASET: " + str(dataset)
print "NUM TOPICS: " + str(n_topic_created)
print "METHOD: " + method
print "Valutazione delle distribuzioni create in corso."
evalMethod.eval_method(dataset, n_topic_created, method, value_method)
print "Valutazione completata."
print "Evaluation.py finish."


