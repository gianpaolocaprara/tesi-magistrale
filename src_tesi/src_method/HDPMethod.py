from pymongo import MongoClient, ASCENDING

client = MongoClient('localhost', 27017)

def create_inverted_components_distribution(dataset, topic_components):
    db = client[dataset]
    components_topic_distribution_HDP_coll = db['components_topic_distribution_HDP']
    components_topic_distribution_HDP_coll.remove({})
    print "Creazione collezione distribuzione inversa delle componenti in corso."
    inverted_component_distribution = {}
    for element in topic_components:
        for item in element[1]:
            try:
                inverted_component_distribution[item[0]].append({'topic': element[0], 'distribution': item[1]})
            except KeyError:
                inverted_component_distribution[item[0]] = [{'topic': element[0], 'distribution': item[1]}]
    for k, v in inverted_component_distribution.items():
        json_to_insert = {'component': k, 'topics_distribution': v}
        components_topic_distribution_HDP_coll.insert_one(json_to_insert).inserted_id
    print "Creazione collezione completata."
    print "Creazione indici."
    components_topic_distribution_HDP_coll.create_index([("component", ASCENDING)])
    components_topic_distribution_HDP_coll.create_index([("topics_distribution", ASCENDING)])
    components_topic_distribution_HDP_coll.create_index([("topics_distribution.distribution", ASCENDING)])
    return


def create_topic_objects_distribution(dataset, corpus):
    db = client[dataset]
    topics_objects_distribution_coll = db['objects_topic_distribution_HDP']
    topics_objects_distribution_coll.remove({})
    print "Creazione collezione distribuzione inversa degli oggetti in corso."
    topic_objects_distribution = {}
    i = 0
    for element in corpus:
        print "Collezione distributioni oggetti, divisi per topic; Iteration: " + str(i)
        # recupero l'ID ristorante associato all'indice
        business_id = get_id_business(dataset, i)
        # incremento il contatore per il prossimo ID
        i += 1
        # scorro la lista delle distribuzioni dei topic del business e aggiungo ogni topic al dizionario
        for tuples in element:
            try:
                topic_objects_distribution[business_id].append({'topic': tuples[0], 'distribution': tuples[1]})
            except KeyError:
                topic_objects_distribution[business_id] = [{'topic': tuples[0], 'distribution': tuples[1]}]
    for k, v in topic_objects_distribution.items():
        json_to_insert = {'idRif': k, 'topics_distribution': v}
        topics_objects_distribution_coll.insert_one(json_to_insert).inserted_id
    print "Creazione collezione completata."
    print "Creazione indici."
    topics_objects_distribution_coll.create_index([("business_distribution.topic", ASCENDING)])
    print "Creazione indici completata."
    return

# ottiene ID del Business data la posizione
def get_id_business(dataset, position):
    db = client[dataset]
    position_business_in_corpora_coll = db['positionBusinessInCorpora']
    business_id = ""
    cursor = position_business_in_corpora_coll.find({"positionInCorpora": position})
    for elem in cursor:
        business_id = elem['business_id']
    return business_id