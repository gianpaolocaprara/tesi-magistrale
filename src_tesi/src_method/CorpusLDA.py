from gensim import corpora
from pymongo import MongoClient
from pymongo import ASCENDING

# classe che crea il corpus per LDA


class CorpusLDA(object):
    def __init__(self, dataset=''):
        self.file_name = 'dict/dictionary_LDA.dict'
        self.dict = corpora.Dictionary.load(self.file_name)
        self.client = MongoClient('localhost', 27017)
        self.db = self.client[dataset]

    def __iter__(self):
        self.createReviewDivededByID()
        reviewDivededByIDReview_coll = self.db['votesDivededByIDRif']
        pipeline = [
            {"$sort": {"_id": 1}}
        ]
        cursor_results = reviewDivededByIDReview_coll.aggregate(pipeline)
        for results in cursor_results:
            string_resturant = "".join([(str(element['idSource'] + " ") * element['count'])
                                        for element in results['sources_counts']])
            yield self.dict.doc2bow(string_resturant.split())

    def createReviewDivededByID(self):
        self.db.command(
            {"aggregate":
                 "DB_Content",
             "pipeline":
                 [{"$group":
                       {"_id":
                            "$idRif", "sources_counts":
                           {"$push":
                                {"count": "$cnt", "idSource": "$idSource"}}}},
                  {"$out": "votesDivededByIDRif"}], "allowDiskUse": True})
        database = self.db["votesDivededByIDRif"]
        database.create_index([("idRif", ASCENDING)])
        return
