from gensim import models

import sys
import Utility, CorporaDictionary, CategoryTM, CntDictionary, LDAMethod, HDPMethod

'''
compute.py : script che permette di computare, dato un dataset con 2 collezioni aventi un particolare formato,
  len(categories) distribuzioni costruite in base al metodo sviluppato con base Gibbs. Vengono passati come parametri:
    1) Dataset da considerare per la computazione
    2) n_topic da generare
    3) Iterazioni da effettuare per il metodo e per LDA
    4) topics_output : topic da considerare quando viene effettuato il salvataggio sul DB
        dei risultati del metodo
Usage: python compute.py dataset n_topic iterations topics_to_consider
'''
if len(sys.argv) < 4:
    print "Bad command. Usage:"
    print "python compute.py dataset n_topic iterations topics_to_consider"
    sys.exit(1)

arguments = sys.argv
dataset = arguments[1]
n_topics = arguments[2]
iterations = arguments[3]
topics_output = arguments[4]

print "*****COMPUTE******"
print "DATASET: " + str(dataset)
print "NUM TOPICS: " + str(n_topics)
print "ITERATIONS: " + str(iterations)
print "TOPICS TO CONSIDER: " + str(topics_output)
print "Creazione dizionari."
Utility.create_dictionary(dataset)
# carica gli oggetti
dictionary_objects, dictionary_components, dictionary_categories = Utility.load()
rows_content = Utility.content_rows(dataset)
print "Creazione dizionario utente-voto (o parola-conteggio)"
cnt_dictionary = CntDictionary.CntDictionary(dataset=rows_content, dict_objects=dictionary_objects, dict_components=dictionary_components)\
    .cntDict
print "Creazione dizionario completato."
print "Creazione corpora."
corpora_dictionary = CorporaDictionary.CorporaDictionary(dataset=rows_content, dict_objects=dictionary_objects
                                                         , dict_components=dictionary_components).corporaDict
print "Creazione corpora completato."

# facciamo partire il metodo
print "Metodo tenendo conto delle categorie in corso..."
corpus_mymethod = CategoryTM.CategoryTM(corpora_dict=corpora_dictionary, cnt_dict=cnt_dictionary
                                    , num_topics=int(n_topics), iterations=int(iterations), dat_to_save=dataset
                                    , topics_to_consider=int(topics_output))
print "Metodo completato."

# calcolo LDA
print "Calcolo LDA puro in corso."
# crea gli oggetti che serviranno per l'analisi
LDAMethod.create_corpora(dataset, 'dict/dictionary_LDA.dict', 'corpus/corpusLDA.mm')

# carica gli oggetti
dict_LDA, training_corpus = LDAMethod.load('dict/dictionary_LDA.dict', 'corpus/corpusLDA.mm')
print "Calcolo LDA in corso."
corpus_lda_model = models.ldamodel.LdaModel(training_corpus, id2word=dict_LDA, num_topics=int(n_topics)
                                            , iterations=int(iterations))
corpus_lda = corpus_lda_model[training_corpus]
print "Calcolo LDA completato."

print "Calcolo distribuzione topic sulle componenti."
topic_users = corpus_lda_model.show_topics(num_topics=n_topics, num_words=5, formatted=False)
print "Calcolo completato. Creazione indice inverso in corso."
LDAMethod.create_inverted_components_distribution(dataset, topic_users)
LDAMethod.create_topic_objects_distribution(dataset, corpus_lda)

print "Calcolo HDP puro in corso"
corpus_hdp_model = models.HdpModel(training_corpus,id2word=dict_LDA)
corpus_hdp = corpus_hdp_model[training_corpus]
print "Calcolo HDP completato."

print "Calcolo distribuzione topic sulle componenti."
topic_users = corpus_hdp_model.show_topics(topics=n_topics, topn=5, formatted=False)
print "Calcolo completato. Creazione indice inverso HDP in corso."
HDPMethod.create_inverted_components_distribution(dataset, topic_users)
HDPMethod.create_topic_objects_distribution(dataset, corpus_hdp)
print "Processo completato."
print "Compute.py finish."

