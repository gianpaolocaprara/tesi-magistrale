from __future__ import division
from pymongo import MongoClient
from collections import Counter, OrderedDict
from gini import gini
from scipy import stats
from sklearn.preprocessing import normalize
from scipy.interpolate import spline
import numpy as n
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def calculate_values(topic, omo_dict, coll_to_save_kde, coll_to_save_gini, path_to_save_charts):
    labelfont = {'fontname': 'Adobe Arabic'}
    titlefont = {'fontname': 'Cambria'}
    sortered_dict = OrderedDict(sorted(omo_dict.items()))

    fig = plt.figure(figsize=(20, 10))

    # 1st: create histogram
    red_patch = mpatches.Patch(color='red', label='Maximum Category Homogeneity')
    grey_patch = mpatches.Patch(color='grey', label='Category Homogeneity')
    plt.legend(handles=[red_patch, grey_patch])
    clrs = ['grey' if (x < max(sortered_dict.values())) else 'red' for x in sortered_dict.values()]
    plt.bar(range(len(sortered_dict)), sortered_dict.values(),
            align='center', color=clrs)
    plt.xticks(range(len(sortered_dict)), sortered_dict.keys())
    plt.title("Topic # " + str(topic) + ": Topic Homogeneity Histogram", fontsize=24, **titlefont)
    plt.xlabel("Categories", fontsize=32, **labelfont)
    plt.ylabel("Topic Homogeneity", fontsize=32, **labelfont)
    # save bar histogram
    plt.savefig(path_to_save_charts + "_histogram_" + str(topic) + ".png", format='png', dpi=100)
    plt.close('all')

    # 2nd: create kernel density
    plt.figure(figsize=(20, 10))

    kde_list = n.array(sortered_dict.values())

    try:
        v = normalize(kde_list[:, n.newaxis], axis=0).ravel()
        density = stats.gaussian_kde(v)

        factor = float(len(v))/20
        xs = n.arange(-1 * factor, factor, .1)
        # print density(xs)
        x_smooth = n.linspace(xs.min(), xs.max(), 200)
        y_smooth = spline(xs, density(xs), x_smooth)

        red_patch = mpatches.Patch(color='red', label='KDE for Topic #' + str(topic))

        plt.legend(handles=[red_patch])
        plt.plot(x_smooth, y_smooth, color='red')
        # plt.plot(xs, density(xs), color='red')
        plt.title("Topic # " + str(topic) + ": Kernel Density Estimation", fontsize=24, **titlefont)
        plt.ylabel("KDE", fontsize=32, **labelfont)

        # maxim_p = max(density(xs))
        maxim_p = max(y_smooth)

        # save kde plot
        plt.savefig(path_to_save_charts + "_kde_" + str(topic) + ".png", format='png', dpi=100)
    except n.linalg.LinAlgError:
        maxim_p = 5
    except TypeError:
        maxim_p = 5

    plt.close('all')

    # 3rd: calculate gini coefficient
    gini_c = gini(n.array(omo_dict.values()))

    # 4rd: save results on MongoDB
    coll_to_save_kde.insert_one({'topic': str(topic), 'kde': maxim_p})
    coll_to_save_gini.insert_one({'topic': str(topic), 'gini': gini_c})

    return maxim_p, gini_c


def plt_final_results(db, value_method):
    # Font grafici
    labelfont = {'fontname':'Adobe Arabic'}
    titlefont = {'fontname':'Cambria'}

    # KDE
    # for LDA
    coll = db['evaluation_LDA_topic_KDE_'+ value_method]
    cursor = coll.find()
    total_kde_lda = 0
    total = 0
    for element in cursor:
        total_kde_lda += element['kde']
        total += 1
    avg_lda = total_kde_lda/total

    # for HDP
    coll = db['evaluation_HDP_topic_KDE_'+ value_method]
    cursor = coll.find()
    total_kde_hdp = 0
    total = 0
    for element in cursor:
        total_kde_hdp += element['kde']
        total += 1
    avg_hdp = total_kde_hdp/total

    # for CATTM
    coll = db['evaluation_CatTM_topic_KDE_'+ value_method]
    cursor = coll.find()
    total_kde_cattm = 0
    total = 0
    for element in cursor:
        total_kde_cattm += element['kde']
        total += 1
    avg_cattm = total_kde_cattm/total

    values_kde = [avg_lda, avg_hdp, avg_cattm]
    plt.figure(figsize=(20, 10))

    red_patch = mpatches.Patch(color='red', label='Maximum KDE Average')
    grey_patch = mpatches.Patch(color='grey', label='KDE Average')

    sticks = ['LDA', 'HDP', 'CatTM']

    clrs_kde = ['grey' if (x < max(values_kde)) else 'red' for x in values_kde]
    plt.bar(range(len(sticks)), values_kde, alpha=0.5, color=clrs_kde, align='center')
    plt.xticks(range(len(sticks)), sticks)
    plt.title("Final Results", fontsize=24, **titlefont)
    plt.ylabel("KDE Average", fontsize=32, **labelfont)
    plt.legend(handles=[red_patch, grey_patch], loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=5)
    plt.savefig('results/kde_final_' + value_method + ".png", format='png', dpi=100)
    plt.close('all')
    # GINI
    # for LDA
    coll = db['evaluation_LDA_topic_GINI_'+ value_method]
    cursor = coll.find()
    total_gini_lda = 0
    total = 0
    for element in cursor:
        total_gini_lda += element['gini']
        total += 1
    avg_lda = total_gini_lda/total

    # for HDP
    coll = db['evaluation_HDP_topic_GINI_'+ value_method]
    cursor = coll.find()
    total_gini_hdp = 0
    total = 0
    for element in cursor:
        total_gini_hdp += element['gini']
        total += 1
    avg_hdp = total_gini_hdp/total

    # for CATTM
    coll = db['evaluation_CatTM_topic_GINI_'+ value_method]
    cursor = coll.find()
    total_gini_cattm = 0
    total = 0
    for element in cursor:
        total_gini_cattm += element['gini']
        total += 1
    avg_cattm = total_gini_cattm/total

    values_gini = [avg_lda, avg_hdp, avg_cattm]
    plt.figure(figsize=(20, 10))

    red_patch = mpatches.Patch(color='red', label='Maximum GINI Average')
    grey_patch = mpatches.Patch(color='grey', label='GINI Average')

    sticks = ['LDA', 'HDP', 'CatTM']

    clrs_gini = ['grey' if (x < max(values_gini)) else 'red' for x in values_gini]
    plt.bar(range(len(sticks)), values_gini, alpha=0.5, color=clrs_gini, align='center')
    plt.xticks(range(len(sticks)), sticks)
    plt.title("Final Results", fontsize=24, **titlefont)
    plt.ylabel("GINI Average", fontsize=32, **labelfont)
    plt.legend(handles=[red_patch, grey_patch], loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=5)
    plt.savefig('results/gini_final_' + value_method + ".png", format='png', dpi=100)
    plt.close('all')

    return


def final_plot_kde_topics(dict_kde, path, dict_gini, path2):
    labelfont = {'fontname': 'Adobe Arabic'}
    titlefont = {'fontname': 'Cambria'}
    fig = plt.figure(figsize=(20, 10))

    red_patch = mpatches.Patch(color='red', label='Maximum KDE Density')
    grey_patch = mpatches.Patch(color='grey', label='KDE Density')
    plt.legend(handles=[red_patch, grey_patch])
    clrs = ['grey' if (x < max(dict_kde.values())) else 'red' for x in dict_kde.values()]

    plt.bar(range(len(dict_kde)), dict_kde.values(), align='center', width=0.3, color=clrs)
    plt.xticks(range(len(dict_kde)), dict_kde.keys())
    plt.title("KDE Topics Histogram", fontsize=24, **titlefont)
    plt.xlabel("Topics", fontsize=32, **labelfont)
    plt.ylabel("Maximum Kernel Density", fontsize=32, **labelfont)

    # save bar histogram
    plt.savefig(path, format='png', dpi=100)
    plt.close('all')

    plt.figure(figsize=(20, 10))
    red_patch = mpatches.Patch(color='red', label='Maximum GINI Coefficient')
    grey_patch = mpatches.Patch(color='grey', label='GINI Coefficient')
    plt.legend(handles=[red_patch, grey_patch])
    clrs = ['grey' if (x < max(dict_gini.values())) else 'red' for x in dict_gini.values()]

    plt.bar(range(len(dict_gini)), dict_gini.values(), align='center', width=0.3, color=clrs)
    plt.xticks(range(len(dict_gini)), dict_gini.keys())
    plt.title("GINI Topics Histogram", fontsize=24, **titlefont)
    plt.xlabel("Topics", fontsize=32, **labelfont)
    plt.ylabel("Maximum GINI Coefficient", fontsize=32, **labelfont)

    # save bar histogram
    plt.savefig(path2, format='png', dpi=100)
    plt.close('all')
    return

def eval_method(dataset, n_topic, method, value_method):
    client = MongoClient('localhost', 27017)
    db = client[dataset]

    # per poter tenere traccia del numero totale dei documenti, divisi per categoria
    category_num = dict()
    # per recuperare le categorie dei documenti/locali
    db_categories = db['DB_Categories']
    pipeline = [{"$group": {"_id": "$category"}}]
    categories = db_categories.aggregate(pipeline)
    for cat in categories:
        category = str(cat['_id'])
        number = db_categories.find({'category': category}).count()
        category_num[category] = number
    categories.close()

    # dizionario per ternere traccia, per ogni topic, dei documenti appartententi al topic
    dict_LDA = {}

    pipeline_lda = [
        {"$unwind": "$topics_distribution"},
        {"$sort": {"idRif": 1, "topics_distribution.distribution": -1}},
        {"$group": {"_id": "$idRif", "topics_distribution": {"$push": "$topics_distribution"}}},
        {"$out": "collection_for_eval_LDA"},
        ]

    db.command(
            {"aggregate":
                    "objects_topic_distribution_LDA",
                "pipeline":
                    pipeline_lda, "allowDiskUse": True})

    db_4lda = db['collection_for_eval_LDA']
    results = db_4lda.find()

    for element in results:
        id_rif = element['_id']
        topics_distribution = element['topics_distribution']

        if method == "topic":
            topics = topics_distribution[:int(value_method)]
            # salvo i risultati sul dizionario
            for topic in topics:
                try:
                    dict_LDA[topic['topic']].append(id_rif)
                except KeyError:
                    dict_LDA[topic['topic']] = [id_rif]
        elif method == "soglia":
            for topic in topics_distribution:
                if topic['distribution'] > float(value_method):
                    try:
                        dict_LDA[topic['topic']].append(id_rif)
                    except KeyError:
                        dict_LDA[topic['topic']] = [id_rif]
        else:
            raise ValueError("Metodo non accettato.")

    results.close()

    eval_kde = db['evaluation_LDA_topic_KDE_' + str(value_method)]
    eval_gini = db['evaluation_LDA_topic_GINI_' + str(value_method)]
    eval_kde.remove()
    eval_gini.remove()

    # for each topic
    kde_list = dict()
    gini_dict = dict()
    for k, v in dict_LDA.items():
        omo_dict_topic = dict()
        cat_list = []
        for id_rif in v:
            result = db_categories.find_one({'idRif': id_rif})
            cat_list.append(result['category'])
        c = Counter(cat_list)

        # for each category
        for key, value in c.items():
            omo_dict_topic[str(key)] = \
                float("{0:.5f}".format((value/sum(c.values()))*100))
            # calculate histogram, kde and gini coefficient

        # print "DIZIONARIO " + str(omo_dict_topic) + "topic #" + str(k)
        max_kde, gini_c = calculate_values(k, omo_dict_topic,
                                           eval_kde, eval_gini,
                                           'results/LDA_topic_' +
                                           str(method) + "_" +
                                           str(value_method))
        # max_kde, gini_c = calculate_values(k, omo_dict_topic,
        #                                    eval_kde, eval_gini,
        #                                    'results/LDA_topic_' +
        #                                    str(method) + "_" +
        #                                    str(value_method))
        kde_list[k] = max_kde
        gini_dict[k] = gini_c

    final_plot_kde_topics(kde_list, 'results/kde_topics_hist_LDA_'+
                          str(value_method)+'.png', gini_dict,
                          'results/gini_topics_hist_LDA_'+
                          str(value_method)+'.png')
    #
    # dizionario per ternere traccia, per ogni topic, dei documenti appartententi al topic
    dict_HDP = {}

    pipeline_hdp = [
            {"$unwind": "$topics_distribution"},
            {"$sort": {"idRif": 1, "topics_distribution.distribution": -1}},
            {"$group": {"_id": "$idRif", "topics_distribution": {"$push": "$topics_distribution"}}},
            {"$out": "collection_for_eval_HDP"},
            ]

    db.command(
                {"aggregate":
                     "objects_topic_distribution_HDP",
                 "pipeline":
                     pipeline_hdp, "allowDiskUse": True})

    db_4hdp = db['collection_for_eval_HDP']
    results = db_4hdp.find()

    for element in results:
        id_rif = element['_id']
        topics_distribution = element['topics_distribution']

        if method == "topic":
            topics = topics_distribution[:int(value_method)]
            # salvo i risultati sul dizionario
            for topic in topics:
                try:
                    dict_HDP[topic['topic']].append(id_rif)
                except KeyError:
                    dict_HDP[topic['topic']] = [id_rif]
        elif method == "soglia":
            for topic in topics_distribution:
                if topic['distribution'] > float(value_method):
                    try:
                        dict_HDP[topic['topic']].append(id_rif)
                    except KeyError:
                        dict_HDP[topic['topic']] = [id_rif]
        else:
            raise ValueError("Metodo non accettato.")

    results.close()

    eval_kde = db['evaluation_HDP_topic_KDE_' + str(value_method)]
    eval_gini = db['evaluation_HDP_topic_GINI_' + str(value_method)]
    eval_kde.remove()
    eval_gini.remove()

    kde_list = dict()
    gini_dict = dict()
    for k, v in dict_HDP.items():
        omo_dict_topic = dict()
        # per limitare topic di HDP
        if k < n_topic:
            cat_list = []
            for id_rif in v:
                # result = db_categories.find_one({'_id': id_rif})
                result = db_categories.find_one({'idRif': id_rif})
                cat_list.append(result['category'])
            c = Counter(cat_list)
            for key, value in c.items():
                # recupero numero degli oggetti della categoria
                # cat_n = category_num[key]
                # homogenity_cat = float("{0:.5f}".format((value/cat_n)*100))
                omo_dict_topic[str(key)] = float("{0:.5f}".format((value/sum(c.values()))*100))
                # calculate histogram, kde and gini coefficient
            # print "DIZIONARIO " + str(omo_dict_topic) + "topic #" + str(k)
            max_kde, gini_c = calculate_values(k, omo_dict_topic, eval_kde, eval_gini,
                            'results/HDP_topic_' + str(method) + "_" + str(value_method))
            # max_kde, gini_c = calculate_values(k, omo_dict_topic, eval_kde, eval_gini,
            #                 'results/HDP_topic_' + str(method) + "_" + str(value_method))
            kde_list[k] = max_kde
            gini_dict[k] = gini_c

    final_plot_kde_topics(kde_list, 'results/kde_topics_hist_HDP_'+ str(value_method)+'.png', gini_dict,
                          'results/gini_topics_hist_HDP_'+ str(value_method)+'.png')

    pipeline = [{"$group": {"_id": "$category"}}]
    categories = db_categories.aggregate(pipeline)

    eval_kde = db['evaluation_CatTM_topic_KDE_' + str(value_method)]
    eval_gini = db['evaluation_CatTM_topic_GINI_' + str(value_method)]
    eval_kde.remove()
    eval_gini.remove()

    for element in categories:
        category = element['_id']
        # db_product_method = db["product_topic_distribution_" + category]
        db_product_method = db["objects_topic_distribution_" + category]

        # dizionario per ternere traccia, per ogni topic, dei documenti appartententi al topic
        dict_category = {}
        results = db_product_method.find()
        for result in results:
            id_rif = result['_id']
            topics_distribution = result['object_distributions']
            # topics_distribution = result['product_distributions']
            if method == "topic":
                ssorted_topics = sorted(topics_distribution, key=lambda x: x['distribution'], reverse=True)
                topics = ssorted_topics[:int(value_method)]
                # salvo i risultati sul dizionario
                for topic in topics:
                    try:
                        dict_category[topic['topic']].append(id_rif)
                    except KeyError:
                        dict_category[topic['topic']] = [id_rif]
            elif method == "soglia":
                for topic in topics_distribution:
                    if topic['distribution'] > float(value_method):
                        try:
                            dict_category[topic['topic']].append(id_rif)
                        except KeyError:
                            dict_category[topic['topic']] = [id_rif]
            else:
                    raise ValueError("Metodo non accettato.")

        categories.close()

        kde_list = dict()
        gini_dict = dict()
        for k, v in dict_category.items():
            omo_dict_topic = dict()
            cat_list = []
            for id_rif in v:
                # result = db_categories.find_one({'_id': id_rif})
                result = db_categories.find_one({'idRif': id_rif})
                cat_list.append(result['category'])
            c = Counter(cat_list)
            for key, value in c.items():
                # recupero numero degli oggetti della categoria
                # cat_n = category_num[key]
                # homogenity_cat = float("{0:.5f}".format((value/cat_n)*100))
                omo_dict_topic[str(key)] = float("{0:.5f}".format((value/sum(c.values()))*100))
                # omo_dict_topic[str(key)] = float("{0:.5f}".format((value/sum(c.values()))*100))
                # calculate histogram, kde and gini coefficient
            max_kde, gini_c = calculate_values(k, omo_dict_topic, eval_kde, eval_gini,
                            'results/CatTM_topic_' + str(method) + "_" + str(value_method) + "_" + str(category))
            kde_list[k] = max_kde
            gini_dict[k] = gini_c

        final_plot_kde_topics(kde_list, 'results/kde_topics_hist_' + str(category) + '_'+ str(value_method)+'.png',
                              gini_dict, 'results/gini_topics_hist_' + str(category) + '_'+ str(value_method)+'.png')

    plt_final_results(db, str(value_method))
    return
